package nl.jsprengers.pbt;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import net.jqwik.api.constraints.Negative;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

class BenefitCalculatorPropertyTest {

    BenefitCalculator calculator = new BenefitCalculator();

    @Property
    public void for_every_input_greater_than_125_the_function_throws(@ForAll @IntRange(min = 126) int age) {
        assertThatThrownBy(() -> calculator.getBenefitInEurosForAge(age));
    }

    @Property
    public void for_every_input_less_than_zero_the_function_throws(@ForAll @Negative int age) {
        assertThatThrownBy(() -> calculator.getBenefitInEurosForAge(age));
    }

    @Property
    public boolean any_input_between_0_and_17_returns_0(@ForAll @IntRange(max = 17) int age) {
        return calculator.getBenefitInEurosForAge(age) == 0;
    }

    @Property
    public boolean any_input_between_18_and_125_returns_200(@ForAll @IntRange(min = 18, max = 125) int age) {
        return calculator.getBenefitInEurosForAge(age) == 200;
    }
}
