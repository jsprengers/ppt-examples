package nl.jsprengers.pbt;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class EnhancedBenefitCalculatorParameterizedTest {

    EnhancedBenefitCalculator calculator = new EnhancedBenefitCalculator();

    @ParameterizedTest
    @CsvSource({"0,-1", "125,126"})
    public void any_age_between_zero_and_125_is_value(int inRange, int outOfRange) {
        calculator.getBenefitInEurosForAge(inRange);
        assertThatThrownBy(() -> calculator.getBenefitInEurosForAge(outOfRange)).hasMessageStartingWith("Age is out of range");
    }

    @ParameterizedTest
    @CsvSource({"17,0", "18,200", "39,200", "40,300", "64,300", "65,200"})
    public void benefit_is_zero_for_minors(int age, int benefit) {
        assertThat(calculator.getBenefitInEurosForAge(age)).isEqualTo(benefit);
    }


}
