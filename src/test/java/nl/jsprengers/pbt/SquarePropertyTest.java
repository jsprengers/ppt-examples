package nl.jsprengers.pbt;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;

public class SquarePropertyTest {

    @Property
    public boolean all_int_ranges_are_valid(@ForAll int input){
        return NumberUtils.square(input) >= 0;
    }

}
