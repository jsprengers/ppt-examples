package nl.jsprengers.pbt;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import net.jqwik.api.constraints.Negative;
import org.assertj.core.api.Assertions;

class EnhancedBenefitCalculatorPropertyTest {

    EnhancedBenefitCalculator calculator = new EnhancedBenefitCalculator();

    @Property
    public void any_age_between_zero_and_125_is_value(@ForAll @IntRange(max = 125) int age) {
        calculator.getBenefitInEurosForAge(age);
    }

    @Property
    public void age_greater_than_125_throws(@ForAll @IntRange(min = 126) int age) {
        Assertions.assertThatThrownBy(() -> calculator.getBenefitInEurosForAge(age));
    }

    @Property
    public void age_less_than_zero_throws(@ForAll @Negative int age) {
        Assertions.assertThatThrownBy(() -> calculator.getBenefitInEurosForAge(age));
    }

    @Property
    public boolean benefit_is_zero_for_minors(@ForAll @IntRange(max = 17) int age) {
        return calculator.getBenefitInEurosForAge(age) == 0;
    }

    @Property
    public boolean benefit_is_200_for_adults_under_40(@ForAll @IntRange(min = 18, max = 39) int age) {
        return calculator.getBenefitInEurosForAge(age) == 200;
    }

    @Property
    public boolean benefit_is_200_for_adults_over_65(@ForAll @IntRange(min = 65, max = 125) int age) {
        return calculator.getBenefitInEurosForAge(age) == 200;
    }

    @Property
    public boolean benefit_is_300_for_adults_between_40_and_65(@ForAll @IntRange(min = 40, max = 64) int age) {
        return calculator.getBenefitInEurosForAge(age) == 300;
    }
}
