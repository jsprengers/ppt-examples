package nl.jsprengers.pbt;

public class BenefitCalculator {
    int getBenefitInEurosForAge(int age) {
        if (age < 0 || age > 125)
            throw new IllegalArgumentException("Age is out of range [0-125]: " + age);
        return age < 18 ? 0 : 200;
    }
}
