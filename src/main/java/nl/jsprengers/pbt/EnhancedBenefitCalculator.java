package nl.jsprengers.pbt;

public class EnhancedBenefitCalculator {
    int getBenefitInEurosForAge(int age) {
        if (age < 0 || age > 125) {
            throw new IllegalArgumentException("Age is out of range [0-125]: " + age);
        } else if (age < 18) {
            return 0;
        } else if (age >= 40 && age < 65) {
            return 300;
        } else {
            return 200;
        }
    }
}
