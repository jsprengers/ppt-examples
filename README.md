# Property Based testing with jqwik

A simple example project to illustrate usage of the jqwik testing utility in comparison with Junit parameterized tests.

Accompanies the article on Dzone: 

Usage:
```sql
maven test
```
